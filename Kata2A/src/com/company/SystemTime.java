package com.company;

import java.util.*;
import java.text. *;

public class SystemTime {
    public static void main(String[] args) {
        // Get current date and time
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        System.out.println(sdf.format(currentDate));

    }
}
