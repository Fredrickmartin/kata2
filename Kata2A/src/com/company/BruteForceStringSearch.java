package com.company;

import java.io.*;
import java.util.Scanner;

public class BruteForceStringSearch {


    //Your task is to write a function that performs brute-force string searching as described above


    public static int bruteforce( String text, String tobematched){

int length = text.length();
int plength = tobematched.length();

for( int i = 0; i< length - plength; i ++){
    int j = 0;
    while((j < plength) && (text.charAt(i + j) == tobematched.charAt(j))){
        j++;

    }
    if( j == plength){
        return i;
    }

}
return -1;
    }

    public static void main(String[] args) {
        BruteForceStringSearch obj = new BruteForceStringSearch();
        Scanner sc = new Scanner(System.in);

        String text= "I love programming and i do programming";

        String tobematched = " programming";

        int position = obj.bruteforce(text, tobematched);
        int endindex = position +1;
        if(position == -1){

            System.out.println("Pattern is not matched in the text");
        }
        else{
            System.out.println("Found at position:" (position +1));
            System.out.println("End at the position:" (endindex + tobematched.length()));

        }


    }
}
