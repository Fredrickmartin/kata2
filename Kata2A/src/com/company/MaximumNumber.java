
package com.company;

import java.util.Scanner;


public class MaximumNumber {
    public static void main(String[] args) {

        //Exercise 7: Find the maximum
        //Find the largest value in an array of integers. (4344,34,5,566764,63352,6645,3345,4431,55362,42,1)

Scanner in = new Scanner(System.in);

double[] numMax = {4344,34,5,566764,63352,6645,3345,4431,55362,42,1 };

double Max = numMax[0];
for ( int counter = 1; counter < numMax.length; counter ++){

    if (numMax[counter] > Max){

        Max = numMax[counter];

        System.out.println("The highest maximum for the number is : " + Max);

    }
}

    }


}
